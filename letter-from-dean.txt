GRADUATE DIVISION
PHONE: (858) 534-3555
FAX: (858) 534-4304
9500 GILMAN DRIVE
LA JOLLA, CALIFORNIA 92093-0003
 
 
April 30, 2019
TO: Anthony Joseph Aylward
Bioinformatics and Systems Biology

FROM: Paul K. Yu
Interim Dean of the Graduate Division

RE: Support time limit expiration

Dear Anthony Joseph Aylward:

This letter is to remind you that your support time limit expires at the end of
the current quarter. As stated in the first notification you received, you will
no longer be eligible for financial support from any source at UC San Diego
once the current quarter has ended.

Exceptions can only be granted by the Graduate Council when extenuating
circumstances exist.

I am confident that you will be able to meet the deadline and wish you well in
your efforts.

Please see your graduate coordinator with any questions.